= TODO

== Get Neandertard Application Ready

This is about getting at least one Generating Function applied to real
world data.  The assumption is that a Neandertard sample is available,
which genotypes can be called on.  (This assumption is nonsense.)

 * extract bSFS from genomes ✓

 * maximize likelihood ✓

 * wrap with a nice command line interface


== Get Realistic Neandertard Application Ready

The assumption is now the genotypes are useless, but genotype
likelihoods are good, i.e. a Neandertard of realistic depth (maybe 10X),
called with `genomfart` or similar.

 * Read a suitable file format, BCF should be reasonable.

 * Compute block likelihoods conditional on bSFS.
   * Dynamic Programming over each block
   * all Neandertard sites are informative; blocks should have the same
     number of positions where all the other samples are informative
   * see how much of this can be tabulated


== Old Ideas

This list is from a time when symbolic computation looked necessary.

 * see if AD could simplify the CAS
   * the memoized derivatives are AD in tower mode anyway, might as
     well use a library that does it right
   * using symbolic calculation to build the GF and invert the Laplace
     transforms, followed by AD for the actual application,  may be a
     valid strategy
   * could combine the advantages of symbolic evaluation (inverse
     Laplace transform) and AD (sharing of subexpressions, numeric
     algorithms to solve equation systems)

 * debug CAS
   * look for corner cases (empty lists, singleton lists)

 * change representation of Expr to be in normal form by construction

 * improve code generator
   * use better instructions (more mul and less div, sub instead of
     multiplying with (-1), etc.)
   * detect which expressions are actually shared
   * use more registers
     * top-down-greedy might work
     * some unspecified DP scheme might work better
   * use parallel SSE instructions, compute four results in parallel
     * can't use integer registers in log or exp
   * optionally use double precision
     * a better implementation of log/exp is needed

 * add other evaluators
   * compiler for x87 with extended precision
     * it has built in log and exp!
   * (byte-code?) interpreter using libquadmath or libmpf
     * reverse mode AD for the gradient?  could be implemented by
       literally running the program twice, once forward and once
       backward
   * polish the interpreter
     * AD automatically applies to the interpreter, even with weird
       multiprecision data types
     * with AD, expressions aren't crazy huge anymore and the
       interpreter may well be fast enough

 * better printing of expressions
   * common expressions should be named and used by name (let ... in),
     but single-use expressions should be printed inline
   * normalized expressions of common constructions should be printed
     nicely (omit ones and zeroes where possible, turn products with
     reciprocals into quotients, expand constant factors over sums, etc.)
