-- | Here we will apply likelihoods from a generating function to a lie:
-- a Neandertard that is so good that we can call genotypes.
--
-- We will read exactly four heffalumps, merge them into variant calls,
-- split them into blocks such that they have the same number of
-- informative sites, extract their bSFSs, compose them into a
-- likelihood function, and get the MLE.
--
-- Note that blocks have the same number of /informative sites/, but
-- their physical size can vary.  This way, they all have the same
-- expected number of mutations, the bSFSs of the whole genome can be
-- tabulated, and the likelihood function becomes simpler.

module Bsfs where

import BasePrelude                  hiding ( Sum, option )
import Bio.Heffa.Genome
import Bio.Heffa.Lump
import Control.Monad.Primitive             ( PrimMonad )
import Numeric.AD                          ( conjugateGradientAscent )
import Numeric.AD.Mode                     ( Mode, (*^), isKnownZero )
import Options.Applicative
import Streaming

import qualified Data.HashMap.Strict                    as H
import qualified Streaming.Prelude                      as Q
import qualified Data.Vector.Storable                   as W
import qualified Data.Vector.Unboxed.Sized              as U
import qualified Data.Vector.Unboxed.Mutable.Sized      as M

import GenFunc
import Neandertard

-- XXX It's probably worth the effort to support shitty neandertard and
-- chimp genomes; i.e. treat them as haploid and use the generating
-- function of a population that has only one gene in the respective
-- demes.
--
-- XXX Does it ever make sense *not* to fold the bSFS?  As in, do we
-- ever know which allele is ancestral?  This would definitely need
-- additional logic, because it's clearly not the reference allele.

bsfs_options :: Parser (IO ())
bsfs_options = bsfs_main
    <$> strOption (long "european"    <> metavar "HEF" <> help "Heffalump of \"European\" sample")
    <*> strOption (long "african"     <> metavar "HEF" <> help "Heffalump of \"African\" sample")
    <*> strOption (long "neandertard" <> metavar "HEF" <> help "Heffalump of \"Neandertard\" sample")
    <*> strOption (long "chimpanzee"  <> metavar "HEF" <> help "Heffalump of \"Chimpanzee\" sample")

    <*> optional (strOption (short 'r' <> long "reference" <> metavar "FILE" <> help "Read reference from FILE (.2bit)"))
    <*> option auto (short 'l' <> long "block-length" <> value 2000
                               <> metavar "LEN" <> help "Blocks of maximum length LEN")
    <*> option auto (short 'm' <> long "num-observed" <> value 1000
                               <> metavar "NUM" <> help "Exactly NUM observed sites per block")
  where
    bsfs_main eur_hef afr_hef nea_hef pan_hef reference max_physical num_observed = do
        bsfss <- decodeManyRef reference [eur_hef,afr_hef,nea_hef,pan_hef] $ \_ref ->
                 Q.fold_ (\acc v -> H.insertWith (+) v (1::Int) acc) H.empty id .
                 mapped block_to_bsfs .
                 observedBlocks (NumObserved num_observed) (MaxPhysical max_physical) .
                 mergeLumps 0 {- no "outgroups" -}

        -- XXX this is useless, except for debugging
        H.foldrWithKey (\k v z -> putStrLn (shows k " => " ++ show v) >> z) (return ()) bsfss

        -- XXX this is not at all tested
        -- Possible alternatives include 'gradientAscent', the
        -- CG_Descent method by Hager/Zhang, (L)BFGS.
        -- conjugateGradientAscent is merely the most available
        -- optimizer right now.
        mapM_ print $ conjugateGradientAscent (lk_genome bsfss) (initial_model num_observed)

        -- XXX for industrial strength, the optimization should
        -- terminate at some point
        --
        -- XXX given the MLE, we should get confidence intervals from
        -- the Hessian matrix.  requires a PCA.  suitable code might be
        -- in genomfart


-- | Computes the bSFS of a block.  We fold the bSFS, because we can't
-- know the identity of the ancestral allele.

block_to_bsfs :: PrimMonad m => Stream (Of Clump) m r -> m (Of (BSFS 41) r)
block_to_bsfs stream = do v <- M.replicate 0
                          r <- Q.mapM_ (step v) stream
                          (:> r) . BSFS <$> U.unsafeFreeze v
  where
    step v Variants{..} | allObserved c_vars = mapM_ (bump v . snd) c_vars
    step _ _                                 = pure ()

    bump v cs = M.read v i >>= M.write v i . succ
      where
        t = W.foldl' (\acc c -> 3 * acc + to_index c) 0 cs
        i = min t (81 - t)

    -- We already discarded sites where any sample has an N.  Now Ns
    -- count as reference---we're counting mutations, after all.
    to_index (AC _ 0) =  0      -- only reference, assume two reference alleles
    to_index (AC 0 _) =  2      -- only alternative, assume two alternative alleles
    to_index (AC _ _) =  1      -- heterozygous, therefore one alternative allele


-- | Maximize the likelihood for a set of genomes.  Takes the table of
-- BFSFs and an initial guess at the ModelParams, returns a stream of
-- increasingly accurate ModelParams.
--
-- This code contains an important detail:  Under the infinite sites approximation,
-- which we use, some patterns of mutations are impossible.  If at least
-- one block has such a configuration, the likelihood becomes zero, and
-- the log-likelihood becomes negative infinity.  All attempts to
-- optimize it end up in collections of NaNs.  Fortunately, these
-- impossible blocks yield a lkelihood that is so obviously zero, we can
-- detect it cheaply and reliably.

lk_genome :: (Mode a, Floating a) => H.HashMap (BSFS 41) Int -> ModelParams a -> a
lk_genome bsfss mp = H.foldlWithKey' step 0 bsfss
  where
    lk = likelihood_folded (generatingFunction today) mp (exp . mutation_rate)
    step acc bsfs num = if isKnownZero (lk bsfs) then acc else acc + fromIntegral num *^ log (lk bsfs)

-- | Guess at initial model.  If N_e is about 10.000, then the rate 1
-- corresponds to (20000 generation times)^-1.  With a generation time
-- of 25a, that's 500.000 years.  Very roughly, it just needs to be a
-- guess.  The mutation rate per site and generation should be about
-- 10^-6, so the scaled mutation rate per block should be about
-- 20000*num_observed*10^-6.

initial_model :: Int -> ModelParams Double
initial_model num_observed = ModelParams
    { chimp_split_rate   = log 12       -- 6Ma
    , neand_split_rate   = log 0.25     -- 125ka
    , human_split_rate   = log 0.05     -- 25ka
    , contact_rate       = log 0.05     -- 25ka
    , mixing_rate        = log 0.0025   -- about 5% of genes in those 25ka
    , admixture_rate     = log 0.05     -- 25ka
    , extinction_rate    = log 0.1      -- 50ka
    , mutation_rate      = log $ fromIntegral num_observed * 0.2 }

