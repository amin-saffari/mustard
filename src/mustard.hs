{-# LANGUAGE ForeignFunctionInterface #-}
module Main where

import BasePrelude
import Foreign.C.Types                  ( CInt(..) )
import Options.Applicative
import System.Time

import qualified Data.IntMap.Strict             as I
import qualified Data.Vector                    as V
import qualified Data.Vector.Storable           as W
import qualified Data.Vector.Storable.Mutable   as WM

import Asm
import Bsfs
import Cas

-- XXX  Currently no access to parameters, statistics or return value.
foreign import ccall unsafe "cg_user.h cg_descent"
    cg_descent ::
        Ptr Double ->       -- input: starting guess, output: the solution
        CInt ->             -- problem dimension
        Ptr () ->           -- Stats, structure with statistics (see cg_descent.h)
        Ptr () ->           -- UParm, user parameters, NULL = use default parameters
        Double ->           -- grad_tol, /* StopRule = 1: |g|_infty <= max (grad_tol, StopFac*initial |g|_infty) [default]
                            --              StopRule = 0: |g|_infty <= grad_tol(1+|f|) */
        FunPtr FuncLow ->   -- f = valgrad (g,x,a,n) or f = valgrad (0,x,a,n)
        Ptr Float ->        -- a: argument for f
        Ptr Double ->       -- Work, either size 4n work array or NULL
        IO CInt

minimize :: Double -> W.Vector Double -> FunPtr FuncLow -> Ptr Float -> IO (W.Vector Double)
minimize grad_tol args fun py = do
    x <- W.thaw args
    WM.unsafeWith x $ \px ->
            void $ cg_descent px (fromIntegral $ W.length args) nullPtr nullPtr grad_tol fun py nullPtr
    W.unsafeFreeze x

invoke :: FunPtr FuncLow -> Ptr Float -> W.Vector Double -> IO Double
invoke p pa xs = W.unsafeWith xs $ \px -> haskFun p nullPtr px pa (fromIntegral $ W.length xs)

invokeGrad :: FunPtr FuncLow -> Ptr Float -> W.Vector Double -> IO (Double, W.Vector Double)
invokeGrad p pa xs =
    WM.new (W.length xs)                >>= \grad ->
    W.unsafeWith xs                       $ \px ->
    WM.unsafeWith grad                    $ \pg ->
    liftM2 (,) (haskFun p pg px pa (fromIntegral $ W.length xs)) (W.unsafeFreeze grad)


timed :: IO r -> IO r
timed k = do TOD a b <- getClockTime
             r <- k
             TOD c d <- getClockTime
             let t = ((c * 10^(12::Int) + d) - (a * 10^(12::Int) + b)) `div` 10^(9::Int)
             putStrLn $ " (" ++ shows t " ms)"
             return r


main_smoke :: IO ()
main_smoke = do
        -- Log of Rosenbrock banana.  Minimum at (1,1).
        let square x = x * x
        let expr0 = logE $ square (1 - "x") + 100 * square ("y" - square "x")

        let vs    = I.fromList [(0,"x"),(1,"y")]
        let grd   = gradient vs expr0
        let args  = W.fromList [2,3]

        printE vs expr0
        mapM_ (printE vs) grd

        -- printAsm $ compileExprs expr0 grd

        assemble (compileExprs expr0 grd) $ \p pa -> do
            print $ map (evalExpr args) (expr0:grd)
            print $ runEval (mapM evalMemo (expr0:grd)) args
            print =<< evalAsm (compileExprs expr0 grd) args
            print =<< invoke p pa args
            print =<< invokeGrad p pa args
            print =<< minimize 0.0001 args p pa


{-
main_ad :: FilePath -> IO ()
main_ad fn_bsfs = do

        -- the bSFS has two populations, leaves us with something very
        -- simple:
        let gf os ls = phi_simple os ls [[1,1],[3,3]]
        bsfs    <- readBsfs <$> liftIO (readFile fn_bsfs)

        llk     <- gfToLlk2 nopeIO infoIO 2 gf bsfs (V.generate 2 (varE . Name))
        -- let grd =  gradient vs llk

        -- printE vs llk
        -- mapM_ (printE vs) grd
        -- liftIO $ T.putStrLn $ "Free variables: " <> T.unwords params

        let vmax = 1
        let args = W.replicate (1+vmax) 0.5

        timed $ print $ evalExpr args llk
        timed $ print $ runEval (evalMemo llk) args
        timed . print =<< timed (evalAsm (compileExprs llk []) args)

        {- print =<< Opt.optimize Opt.defaultParameters -- { Opt.verbose = Opt.Verbose }
             0.0001 args
             (Opt.VFunction $ runEval (evalMemo llk))
             (Opt.VGradient $ W.fromList . runEval (mapM evalMemo grd))
             (Just . Opt.VCombined $ (head &&& (W.fromList . tail)) . runEval (mapM evalMemo (llk:grd)))

        assemble (compileExprs llk grd) $ \_ f fc -> do
            timed $ print =<< f =<< W.unsafeThaw args

            print =<< Opt.optimize Opt.defaultParameters -- { Opt.verbose = Opt.Verbose }
                 0.0001 args (Opt.MFunction f) (Opt.MGradient $ (.) void . fc) (Just $ Opt.MCombined fc) -}
-}

-- Creates a vector of dummy variables, suitable to be passed to
-- 'phi_simple' and friends.  Argument is the number of populations we
dummies, vars :: Int -> V.Vector Expr
dummies n = V.fromList [varE (Name (-i)) | i <- [1..n]]
vars    n = V.fromList [varE (Name   i ) | i <- [0..(n-1)]]

pops :: Int -> [[Int]]
pops n = [ [l,l] | i <- [0..(n-1)], let l = 3^i ]

main :: IO ()
main = do main' <- execParser opts ; main'
  where
    opts = info (bsfs_options <**> helper)
                (fullDesc <> progDesc "Test of the Lohse method" <> header "mustard - a test for the Lohse method")

-- main = main_smoke
{- main = do main_smoke
          putStrLn "---- 8< ---------------------"
          main_bsfs "testMustard.m" "bar.bsfs" params_4hap
          main_bsfs "twoEADnRXTsimpSort2N-1.m" "foo.bsfs" params_2dip
          main_ad "foo.bsfs" -}

