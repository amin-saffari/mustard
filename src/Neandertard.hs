module Neandertard ( ModelParams(..), generatingFunction, today ) where

import BasePrelude
import Data.Finite
import Data.MemoTrie
import GenFunc
import Numeric.AD.Mode

import qualified Data.Vector.Sized  as V

-- ^ The goal looks like this:  The are four demes (A,B,C,D) of two
-- genes each.  A and B split most recently, after they split from C,
-- and all that after they split from D.  After the most recent split, C
-- admixes into A, and after that, C becomes extinct.  Put another way,
-- A = European, B = African, C = Neandertard, D = Chimpanzee reference
-- genome.
--
-- All demes have two genes, because we assume diploid individuals.  The
-- two genes are indistinguishable, because we assume unphased
-- genotypes.  It's possible that real data has only one gene in a deme,
-- but we can handle that by passing a smaller state to
-- 'generatingFunction'.
--
-- Obviously, the idea is to estimate the magnitude and time of the gene
-- flow, and possibly to date the Neandertard fossil.  Straight forward
-- modifications might allow for the estimation of population sizes.


-- | A branch is defined by a set of genes.  Since genes are labelled by
-- present day tribes, a branch is the same as a set of genes.  Because
-- we always have two genes with the same label, it's really a
-- \"two-bounded multiset\" or something like that.  Just like a set can
-- be represented as a number in binary, our \"two-bounded multiset\" is
-- represented as a number in ternary.  The 'Semigroup' instance
-- computes the branch two given branches merge into.  This will only
-- work if the two branches are disjoint.  (They always are in our
-- application).
newtype Branch = Branch (Finite 81) deriving (Eq, Ord)

instance Semigroup Branch where Branch a <> Branch b = Branch (a+b)

-- | A set of genes.  Represented as a list out of convenience.
newtype Deme = Deme { unpackDeme :: [Branch] } deriving Generic

instance Semigroup Deme where Deme a <> Deme b = Deme (a ++ b)
instance Monoid Deme where mempty = Deme [] ; mappend = (<>)

infixl 5 @+, @-

(@+) :: Deme -> Branch -> Deme
Deme gs @+ b = Deme $ b : gs

(@-) :: Deme -> Branch -> Deme
Deme gs @- b = Deme $ delete b gs


-- | State of the population.  Our population goes through discrete
-- stages, and at each stage, it is represented as multiple collections
-- of genes.
--
-- Note that in state 'Sad', the Neandertard is dead.  We still track
-- its 'Deme', they just don't participate in coalescence.  This
-- results in their branch lengths being shorter, and the fossil not
-- accumulating differences while in the ground.
--
-- This model contains interesting sub-models, however, it does not
-- contain a sub-model without admixture.  The reason is that without
-- the admixture event, the human deme can split after the Neandertard
-- is already extinct.  The corresponding \"sad trio\" state is missing.
--
-- Also note that the encoding seems needlessly complicated.  I consider
-- this worthwhile, because it makes invalid states impossible to
-- represent, and because it should allow for a more compact memo trie.
-- (It also turns out that defining the 'HasTrie' instance is trivial,
-- thanks to generics.)

data Population = Solo  Deme                    -- ^ before the hominini split
                | Duo   Deme Deme               -- ^ after splitting from chimps
                | Trio  Deme Deme Deme          -- ^ after splitting from Neandertards
                | Pure  Deme Deme Deme Deme     -- ^ after the humans split
                | Happy Deme Deme Deme Deme     -- ^ in contact with Neandertards
                | Mixed Deme Deme Deme Deme     -- ^ isolated after contact
                | Sad   Deme Deme Deme Deme     -- ^ after the Neandertard dies out
  deriving Generic

-- | The population today: two european genes in the european deme, two
-- african genes in the african deme, two neandertard genes in the
-- neandertard deme, and one chimp gene in the chimp deme.
--
-- The european genes are in deme 0, therefore on branch \(3^0\), and
-- there are two of them.  The african genes are in deme 1, therefore
-- on branch \(3^1\), and there are two of them, etc.
today :: Population
today = Sad (mkDeme 0) (mkDeme 1) (mkDeme 2) (mkDeme 3)
  where
    mkDeme i = Deme [ Branch i, Branch i ]


instance HasTrie Branch where
  newtype (Branch :->: b) = BranchTrie (V.Vector 81 b)
  trie f = BranchTrie (V.generate (f . Branch))
  untrie (BranchTrie v) (Branch i) = V.index v i
  enumerate (BranchTrie v) = zipWith (\i b -> (Branch i, b)) [0..] (V.toList v)

instance HasTrie Deme where
  newtype (Deme :->: b) = DemeTrie { unDemeTrie :: Reg Deme :->: b }
  trie = trieGeneric DemeTrie
  untrie = untrieGeneric unDemeTrie
  enumerate = enumerateGeneric unDemeTrie

instance HasTrie Population where
  newtype (Population :->: b) = PopulationTrie { unPopulationTrie :: Reg Population :->: b }
  trie = trieGeneric PopulationTrie
  untrie = untrieGeneric unPopulationTrie
  enumerate = enumerateGeneric unPopulationTrie

-- | A weighted sum of components.  Tracks the sum itself, and the sum
-- of weights.
data WeightedSum a = WS { _ws_total :: !a, _ws_weight :: !a }

instance Num a => Semigroup (WeightedSum a) where
    WS a b <> WS c d = WS (a+c) (b+d)

instance Mode t => Monoid (WeightedSum t) where
    mempty = WS zero zero
    mappend = (<>)

getWeightedSum :: (Fractional t, Mode t) => WeightedSum t -> t
getWeightedSum (WS s w) = if isKnownZero s && isKnownZero w then auto 1 else s / w


-- | Parameter set suitable for an 'Population'.  The rate parameters
-- introduced here are all timing parameters for discrete events.  Note
-- that time is rescaled so that the rate of coalescence is always one.
-- (This assumes the same effective population size on all branches.)
--
-- All parameters are actually log-transformed.  Without this, the rates
-- would be constrained to be positive, but constrained optimization is
-- awkward at best.  After the log-transformation, no constraints
-- exists, and the log also has generally better behaved asymptotics.
-- We should be able to get reasonable confidence intervals.

data ModelParams a = ModelParams
    { chimp_split_rate   :: !a      -- ^ measured on the homo branch
    , neand_split_rate   :: !a      -- ^ measured on the human branch
    , human_split_rate   :: !a      -- ^ measured since the admixture
    , contact_rate       :: !a      -- ^ duration of the admixture event
    , mixing_rate        :: !a      -- ^ rate of genes migrating
    , admixture_rate     :: !a      -- ^ rate of admixture events, measured since extinction
    , extinction_rate    :: !a      -- ^ rate of Neandertards extinction
    , mutation_rate      :: !a }    -- ^ uniform, scaled mutation rate
  deriving (Functor, Foldable, Traversable, Show)


-- | The generation function proper.  This is obviously specific to our
-- idea of an 'Population', takes 'ModelParams' appropriate for it,
-- makes assumptions about the number of genes, and hence takes a
-- specific number of dummy parameters.  To fit a different model, it
-- has to be reimplemented.
--
-- Note that when the memo trie is built, the GF is fully applied.  This
-- makes it a trie of values, ensuring that expensive calculations are
-- performed only once.  A trie of functions, which is approximately
-- what Konrad builds in Mathematica, would be less useful, because we
-- would still have to evaluate them repeatedly.

generatingFunction :: forall t . (Floating t, Mode t) => Population -> GF ModelParams 81 t
generatingFunction pop0 mp omegas = untrie tt pop0
  where
    tt = trie $ \pop -> getWeightedSum $ memo_generating_function pop mp omegas tt

type GFBuild t = ModelParams t -> V.Vector 81 t -> Population :->: t -> WeightedSum t

-- An event happening at a certain rate.  Uses the memoized value of
-- the generation function instead of recursive evaluation.
infix 7 .*.
(.*.) :: Mode t => t -> Population -> GFBuild t
r .*. e = \_ _ tt -> WS (r * untrie tt e) r

-- Terminal branches contribute their dummy parameter to the total
-- rate, but no generating function of their own.
terminalBranches :: Mode t => Deme -> GFBuild t
terminalBranches gs _ omegas _ =
    foldMap (\(Branch b) -> WS 0 (V.index omegas b))
            (unpackDeme gs)

coalescences :: Mode t => (Deme -> Population) -> Deme -> GFBuild t
coalescences mkpop gs =
    mconcat [ 1 .*. mkpop (gs @- a @- b @+ (a <> b))
            | a : gs' <- tails $ unpackDeme gs
            , b <- gs' ]

-- | We model gene flow as a continuous process in a finite time window.
-- 'contact_rate' determines the duration of the mixing event, and
-- 'mixing_rate' determines how frequently genes migrate.  The fraction
-- of mixed genes ends up being their ratio.

unmix :: (Floating t, Mode t) => (Deme -> Deme -> Population) -> Deme -> GFBuild t
unmix mkpop gs mps =
    foldMap (\g -> exp (mixing_rate mps) .*. mkpop (gs @- g) (mempty @+ g))
            (unpackDeme gs) mps

-- | Recursively constructs the GF.  Every arm of this function has to
-- do the same basic things:  add the terminal branches to the total
-- rate, and recursively evaluate the states reachable by coalescing any
-- two genes that are currently in the same deme.
--
-- The cases 'Duo', 'Trio', and 'Pure' allow for two demes to join.  The
-- state prior to that is easily constructed by taking the union of the
-- gene sets representing these two demes.  The 'Mixed' case instead
-- invokes 'unmix' to change to 'Pure'.
--
-- The difference between the 'Mixed' and 'Sad' cases is that in the
-- 'Sad' case, the Neandertard doesn't contribute anything.  The only
-- possible event is the extinction of the Neandertard, which leads back
-- to the 'Mixed' case, where the Neandertard genes participate in the
-- regular way.

memo_generating_function :: (Floating t, Mode t) => Population -> GFBuild t
memo_generating_function pop ModelParams{..}  =  gf pop ModelParams{..}
  where
    gf (Solo              hom) = terminalBranches hom <> coalescences Solo hom

    gf (Duo           hom pan) = foldMap terminalBranches [hom, pan] <>
                                 coalescences (`Duo` pan) hom <>
                                 coalescences (hom `Duo`) pan <>
                                 exp chimp_split_rate .*. Solo (hom <> pan)

    gf (Trio      sap nea pan) = foldMap terminalBranches [sap, nea, pan] <>
                                 coalescences (\sap' -> Trio sap' nea pan) sap <>
                                 coalescences (\nea' -> Trio sap nea' pan) nea <>
                                 coalescences (\pan' -> Trio sap nea pan') pan <>
                                 exp neand_split_rate .*. Duo (sap <> nea) pan

    gf (Pure  eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp human_split_rate .*. Trio (eur <> afr) nea pan

    gf (Happy eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 unmix (\eur' nea' -> Happy eur' afr (nea <> nea') pan) eur <>
                                 exp contact_rate .*. Pure eur afr nea pan

    gf (Mixed eur afr nea pan) = foldMap terminalBranches [eur, afr, nea, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\nea' -> Pure eur afr nea' pan) nea <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp admixture_rate .*. Happy eur afr nea pan

    gf (Sad   eur afr nea pan) = foldMap terminalBranches [eur, afr, pan] <>
                                 coalescences (\eur' -> Pure eur' afr nea pan) eur <>
                                 coalescences (\afr' -> Pure eur afr' nea pan) afr <>
                                 coalescences (\pan' -> Pure eur afr nea pan') pan <>
                                 exp extinction_rate .*. Mixed eur afr nea pan

