module Asm where

import BasePrelude hiding ( (:+), getConst, Sum )
import Cas
import Control.Monad.Reader
import Control.Monad.State.Strict
import Foreign.C.Types ( CInt(..) )
import GHC.Float
import Number
import System.IO

import qualified AsmLL
import qualified Data.HashTable.IO              as H
import qualified Data.Vector.Storable           as W
import qualified Data.Vector.Storable.Mutable   as WM
import qualified Data.Sequence                  as Z

-- ^ And another attempt...  Generating LLVM IR didn't work, because it
-- will not manage to compile it with finite resources.  Generating C
-- code failed the same way, because GCC won't compile it with finite
-- resources.  That leaves generating code ourselves.

type TmpMap = H.BasicHashTable (StableName Expr) Temporary
type CstMap = H.BasicHashTable Float Constant

-- | Our (embedded) assembly language.  Different instances can print
-- the assembly (see 'PrintAsm'), generate machine code (see 'Asm') or
-- evaluate it (see 'VM').  Except for the code generator, these are
-- only good for debugging.
--
-- We'll assume a machine with 16 floating point registers, which fits
-- SSE2.  (It can probably be generalized to different numbers, but it's
-- not important right now.)  Instructions are two-address, the result
-- overwrites the first operand.  Some random crap also needs two 32 bit
-- integer registers EAX and ECX.

class MonadIO m => MonadAsm m where
    addR :: Reg -> Reg -> m ()              -- ^ basic arithmetic
    addC :: Reg -> Constant -> m ()
    subR :: Reg -> Reg -> m ()
    mulR :: Reg -> Reg -> m ()
    mulC :: Reg -> Constant -> m ()
    divR :: Reg -> Reg -> m ()
    movR :: Reg -> Reg -> m ()
    maxC :: Reg -> Constant -> m ()
    minC :: Reg -> Constant -> m ()
    flrR :: Reg -> Reg -> m ()              -- ^ computes floor(), i.e. rounds downwards

    movToEax   :: Reg -> m ()               -- ^ reinterpret-cast float to int
    movToEcx   :: Reg -> m ()
    movFromEax :: Reg -> m ()               -- ^ reinterpret-cast int to float

    -- alternative: ANDPS, ORPS
    andEax     :: Int32 -> m ()
    orEax      :: Int32 -> m ()

    -- alternative: CVTDQ2PS -- convert four integers to float (xmm->xmm)
    cvtFromEax :: Reg -> m ()               -- ^ convert int to float
    cvtFromEcx :: Reg -> m ()               -- ^ convert int to float

    -- alternative: CVTTPS2DQ -- convert four floats to integer with truncation (xmm->xmm)
    cvtToEax   :: Reg -> m ()               -- ^ convert float to int, truncating, i.e. rounding towards zero

    allocConst :: Float -> m Constant       -- ^ allocates storage for a constant
    loadC      :: Reg -> Constant -> m ()

    loadArg     :: Reg -> Int -> m ()
    storeResult :: Int -> Reg -> m ()       -- ^ stores a piece of the output
    condRet     :: m ()                     -- ^ return if the output pointer is NULL

    getTmpMap :: m TmpMap
    spillTemp :: Reg -> m Temporary         -- ^ allocates storage and spills a register
    loadTemp  :: Reg -> Temporary -> m ()

-- | Registers are all the same, they are numbered.  Each register
-- stores a 'Float'.  On x86, these would be the SSE registers.
newtype Reg = Reg Word8

instance Show Reg where
    show (Reg i) = "%xmm" ++ show i

-- | Constants live in memory and are allocated sequentially.
newtype Constant = Constant Int

instance Show Constant where
    show (Constant i) = "[" ++ show i ++ "]"

-- | Temporaries live in memory and are allocated sequentially.
newtype Temporary = Temporary Int


-- | Compiles expressions to 'Asm'.
--
-- This takes one primary expression and a list of expressions for the
-- gradient.  In the compiled code, the primary expression is always
-- computed, but the gradient is optional.

compileExprs :: MonadAsm m => Expr -> [Expr] -> m ()
compileExprs fn grd = do
        emitExpr fn
        getExp fn (Reg 0) >> condRet
        zipWithM_ (\i e -> emitExpr e >> getExp e (Reg 0) >> storeResult i (Reg 0)) [0..] grd
        getExp fn (Reg 0)

-- | Emits assembly code for an expression so that its result ends up in
-- register 0.  It calls 'getExp' for any arguments, which causes code
-- for subexpressions to be emitted in turn.  This is very primitive,
-- especially the translation of Sum and Prod.  We incur unnecessary
-- loads of zeroes, multiplications with ±1, and divisions instead of
-- multiplications, but all of this is better fixed with a sensible
-- register allocator.

emit1 :: MonadAsm m => ExprP -> m ()
emit1 (Var (Name p)) = loadArg (Reg 0) p
emit1 (Log   e) = do emitExpr e; getExp e (Reg 0) ; emitLog
emit1 (Exp   e) = do emitExpr e; getExp e (Reg 0) ; emitExp
emit1 (Pow e n) = do emitExpr e; getExp e (Reg 0) ; powi (abs n)
                     when (n<0) $ do movR (Reg 1) (Reg 0) ; allocConst 1 >>= loadC (Reg 0) ; divR (Reg 0) (Reg 1)

emit1 (Sum es) = do mapM_ emitExpr es
                    subR (Reg 0) (Reg 0)
                    mapM_ (\e -> getExp e (Reg 1) >> addR (Reg 0) (Reg 1)) es

emit1 (Prod es c) = do mapM_ emitExpr es
                       allocConst (double2Float $ n2d c) >>= loadC (Reg 0)
                       mapM_ (\e -> getExp e (Reg 1) >> mulR (Reg 0) (Reg 1)) es

-- | Computes (Reg 0) ^ n into (Reg 0) for strictly positive exponents.
-- Uses the russian peasant algorithm (repeated squarings and
-- multiplications).
powi :: MonadAsm m => Int -> m ()
powi 0 = allocConst 1 >>= loadC (Reg 0)
powi 1 = return ()
powi n | even n    = do mulR (Reg 0) (Reg 0)
                        powi (div n 2)
       | otherwise = do movR (Reg 1) (Reg 0)
                        mulR (Reg 1) (Reg 1)
                        powi' (div n 2)

-- | computes (Reg 0) * (Reg 1) ^ n
powi' :: MonadAsm m => Int -> m ()
powi' 1 = mulR (Reg 0) (Reg 1)
powi' n | even    n = do mulR (Reg 1) (Reg 1)
                         powi' (div n 2)
        | otherwise = do mulR (Reg 0) (Reg 1)
                         mulR (Reg 1) (Reg 1)
                         powi' (div n 2)

-- | Makes sure code for expression i is emitted.  If i is already
-- located in a temporary, it does nothing.  Else it emits code for i
-- and spills the result to temporary storage.
emitExpr :: MonadAsm m => Expr -> m ()
emitExpr e = do
    hm <- getTmpMap
    nm <- liftIO $ makeStableName e
    liftIO (H.lookup hm nm) >>= \case
        Just  _ -> return ()
        Nothing -> do emit1 $ prim_expr e
                      ti <- spillTemp (Reg 0)
                      liftIO $ H.insert hm nm ti

-- | Retrieves the result of evaluating expression named i into register
-- r.  It is an error if the expression hasn't been evaluated yet.
getExp :: MonadAsm m => Expr -> Reg -> m ()
getExp e r = do
    hm <- getTmpMap
    nm <- liftIO $ makeStableName e
    liftIO (H.lookup hm nm) >>= \case
        Nothing -> error $ "missing temporary"
        Just  t -> loadTemp r t


-- | Low level functions.  Arguments are pointer to gradient (output),
-- pointer to arguments (input), pointer to memory area (temporaries and
-- constants).  We use 'Double' for easier interfacing to Hager-Zhang.
type FuncLow = Ptr Double -> Ptr Double -> Ptr Float -> CInt -> IO Double

foreign import ccall "dynamic" haskFun :: FunPtr FuncLow -> FuncLow

prologue :: Asm ()
prologue = do flags <- Asm (gets as_flags)
              go $ flags .&. 3
              unless (testBit flags 2) (error "sorry, no SSE2")
  where
    -- No prologue needed on amd64, omitting the frame pointer.
    go 2 = Asm $ liftIO $ putStrLn "prologue for amd64"

    -- Prologue for i386: stack frame, save registers, get args into registers.
    go 1 = do Asm $ liftIO $ putStrLn "prologue for i386"
              emit [ 0x55, 0x56, 0x57 ] -- push   %ebp; %esi; %edi
              emit [ 0x89, 0xe5 ]       -- mov    %esp, %ebp
              emit [ 0x8b, 0x7d, 16 ]   -- mov    16(%ebp), %edi
              emit [ 0x8b, 0x75, 20 ]   -- mov    20(%ebp), %esi
              emit [ 0x8b, 0x55, 24 ]   -- mov    24(%ebp), %edx

    go _ = error "can't assemble"


data WhyExit = FunctionEnds | GradientIsNeeded deriving Show

epilogue :: WhyExit -> Asm ()
epilogue why = Asm (gets as_flags) >>= go . (.&.) 3
  where
    -- Epilogue on amd64: convert from float to double.
    go 2 = do Asm $ liftIO $ putStrLn $ "epilogue for amd64, " ++ show why
              case why of FunctionEnds -> return ()
                          GradientIsNeeded -> do emit [ 0x48, 0x85, 0xff ]      -- test %rdi, %rdi
                                                 emit [ 0x75, 5 ]               -- jne over the early exit
              emit [ 0xf3, 0x0f, 0x5a, 0xc0 ]                                   -- cvtss2sd %xmm0 %xmm0
              emit [ 0xc3 ]                                                     -- ret

    -- Epilogue on i386: restore registers, get result into FPU.
    go 1 = do Asm $ liftIO $ putStrLn $ "epilogue for i386, " ++ show why
              case why of FunctionEnds -> return ()
                          GradientIsNeeded -> do emit [ 0x85, 0xff ]            -- test %edi, %edi
                                                 emit [ 0x75, 12 ]              -- jne over the early exit
              -- move %xmm0 to %st0 via stack
              emit [ 0xf3, 0x0f, 0x11, 0x45, 240 ]                              -- movss %xmm0, -16(%ebp)
              emit [ 0xd9, 0x45, 240 ]                                          -- fld -16(%ebp)
              emit [ 0x5f, 0x5e, 0x5d ]                                         -- pop %edi, pop %esi, pop %ebp
              emit [ 0xc3 ]                                                     -- ret

    go _ = error "can't assemble"


-- | Assembles a program to machine language.  It supplies the platform
-- flags to generate appropriate code, and it adds prologue and epilogue
-- code.  It's important that the assembled code does not return, it
-- _must_ fall off the end.
--
-- XXX  parameterisation of code generator is fucked up
assemble :: Asm () -> (FunPtr FuncLow -> Ptr Float -> IO r) -> IO r
assemble prg k = AsmLL.withBuf
        (\p s    -> do tmp_map <- H.new
                       consts <- H.new
                       flags <- AsmLL.cpu_flags
                       execStateT (runAsm $ prologue >> prg >> epilogue FunctionEnds) $
                            AS p (plusPtr p $ fromIntegral s) 0 consts 0 tmp_map flags)
        (\p _ as -> do temps <- WM.new (as_n_csts as + as_n_temps as)
                       H.mapM_ (\(x, Constant i) -> WM.write temps (as_n_csts as - i - 1) x) (as_consts as)
                       hPutStrLn stderr ("assembled " ++ show (as_front as `minusPtr` p) ++ " bytes, "
                                         ++ show (as_n_csts as) ++ " constants.")
                       WM.unsafeWith temps $ \p_temps ->
                           k (castPtrToFunPtr p) (p_temps `plusPtr` (4 * as_n_csts as)))

-- We generate code for i686+SSE2 or x86_64.  In 32-bit mode, there are
-- 8 SSE registers, in 64-bit mode, there are 16.  (XXX Currently makes
-- no difference.)  If SSE4.1 is present, we use roundss, else a
-- workaround.
--
-- Calling convention in 64bit mode:  Integers and pointers are received
-- in registers: %rdi, %rsi, %rdx, %rcx, %r8, %r9.  %rax, %r10, %r11 can
-- be messed with without precautions.  %xmm0..%xmm15 are available for
-- messing around, the result is passed in %xmm0.
--
-- When generate machine code for x86_64, we receive pointers to the
-- gradient in %rdi, the args in %rsi, the scratch space in %rdx.  We
-- never change these pointers.
--
-- Calling convention in 32bit mode:  Pointers are passed on the stack;
-- %rsi and %rdi are callee-saved, %rdx, %rax, %rcx are scratch.  The
-- float or double result in passed in %st0.  So we need prolog code:
-- - push %rdi and %rsi to the stack
-- - load %rdi, %rsi, %rdx from arguments on the stack
-- and epilog code:
-- - pop %rdi and %rsi from the stack
-- - move %xmm0 to stack and FLD it into %st0
--
-- We allocate temporary slots from 0 upwards and constant slots from
-- (-1) downwards.
--
-- XXX  If we want to parallelize, the result needs to be returned in
-- memory.  If we want to interface to Hager/Zhang, we must follow the
-- calling conventions.  FPU code may actually be faster for serial
-- code?!  Could use extended precision and real log/exp instructions,
-- too...  So many options, so little time...

data AsmState = AS { as_front   :: !(Ptr Word8)              -- code being emitted here
                   , as_end     :: !(Ptr Word8)              -- end of buffer
                   , as_n_csts  :: !Int
                   , as_consts  :: !CstMap
                   , as_n_temps :: !Int
                   , as_temps   :: !TmpMap
                   , as_flags   :: !Int }

newtype Asm a = Asm { runAsm :: StateT AsmState IO a }
    deriving ( Functor, Applicative, Monad, MonadIO )


-- XXX could (should?) check for buffer overflow here
emit :: [Word8] -> Asm ()
emit ws = Asm $ do as <- get
                   lift $ zipWithM_ (pokeElemOff (as_front as)) [0..] ws
                   put $ as { as_front = as_front as `plusPtr` length ws }

emitI32 :: Int -> Asm ()
emitI32 x = emit [ fromIntegral $ shiftR x 0
                 , fromIntegral $ shiftR x 8
                 , fromIntegral $ shiftR x 16
                 , fromIntegral $ shiftR x 24 ]

-- (WTF?  Only eight registers now?  Is there another encoding?  Yes,
-- there is; the last bit is in the prefix byte.  XXX)
-- Also, (almost) all of this could use the parallel versions of these
-- instructions, and we'd get four evaluations at not extra cost.
instance MonadAsm Asm where
    -- could use movaps and get a shorter encoding(?)
    movR (Reg x) (Reg y) = emit [ 0xf3, 0x0f, 0x10, 0xc0 .|. shiftL x 3 .|. y ]             -- movss
    addR (Reg x) (Reg y) = emit [ 0xf3, 0x0f, 0x58, 0xc0 .|. shiftL x 3 .|. y ]             -- addss
    subR (Reg x) (Reg y) = emit [ 0xf3, 0x0f, 0x5c, 0xc0 .|. shiftL x 3 .|. y ]             -- subss
    mulR (Reg x) (Reg y) = emit [ 0xf3, 0x0f, 0x59, 0xc0 .|. shiftL x 3 .|. y ]             -- mulss
    divR (Reg x) (Reg y) = emit [ 0xf3, 0x0f, 0x5e, 0xc0 .|. shiftL x 3 .|. y ]             -- divss
    flrR (Reg x) (Reg y) = do
        has_roundss <- Asm $ gets $ (`testBit` 3) . as_flags
        case has_roundss of
            _ | x == y -> error "flrR: need temp register"
            True       -> emit [ 0x66, 0x0f, 0x3a, 0x0a, 0xc0 .|. shiftL x 3 .|. y, 0x01 ]  -- roundss downwards
            False      -> do
                -- Workaround for missing 'roundss':  needs one working
                -- register, trashes %eax.
                -- x := float( truncate_to_int(x) ) - (x < 0 ? 1 : 0)
                cvtToEax (Reg y)
                emit [ 0x0f, 0x2f, 0xc0 .|. shiftL y 3 .|. x]       -- comiss x y: sets CF if x < 0
                emit [ 0x83, 0xd8, 0 ]                              -- sbb eax 0: eax -= CF
                cvtFromEax (Reg x)

    movToEax   (Reg x) = emit [ 0x66, 0x0f, 0x7e, 0xc0 .|. shiftL x 3 ]
    movToEcx   (Reg x) = emit [ 0x66, 0x0f, 0x7e, 0xc1 .|. shiftL x 3 ]
    movFromEax (Reg x) = emit [ 0x66, 0x0f, 0x6e, 0xc0 .|. shiftL x 3 ]

    cvtToEax   (Reg x) = emit [ 0xf3, 0x0f, 0x2c, 0xc0 .|. x ]                              -- cvttss2si
    cvtFromEax (Reg x) = emit [ 0xf3, 0x0f, 0x2a, 0xc0 .|. shiftL x 3 ]                     -- cvtsi2ss
    cvtFromEcx (Reg x) = emit [ 0xf3, 0x0f, 0x2a, 0xc1 .|. shiftL x 3 ]                     -- cvtsi2ss

    andEax     x = emit [0x25] >> emitI32 (fromIntegral x)
    orEax      x = emit [0x0d] >> emitI32 (fromIntegral x)

    loadC (Reg x) (Constant i) = emit [ 0xf3, 0x0f, 0x10, 0x82 .|. shiftL x 3 ] >> emitI32 (-4*(1+i)) -- movss %x %rdx[-1-i]
    addC  (Reg x) (Constant i) = emit [ 0xf3, 0x0f, 0x58, 0x82 .|. shiftL x 3 ] >> emitI32 (-4*(1+i)) -- addss
    mulC  (Reg x) (Constant i) = emit [ 0xf3, 0x0f, 0x59, 0x82 .|. shiftL x 3 ] >> emitI32 (-4*(1+i)) -- mulss
    maxC  (Reg x) (Constant i) = emit [ 0xf3, 0x0f, 0x5f, 0x82 .|. shiftL x 3 ] >> emitI32 (-4*(1+i)) -- maxss
    minC  (Reg x) (Constant i) = emit [ 0xf3, 0x0f, 0x5d, 0x82 .|. shiftL x 3 ] >> emitI32 (-4*(1+i)) -- minss

    -- note: trashes the register!
    storeResult i (Reg x) = emit [0xf3, 0x0f, 0x5a, 0xc0 + 9*x ] >>                                 -- cvtss2sd %x %x
                            emit [0xf2, 0x0f, 0x11, 0x87 .|. shiftL x 3] >> emitI32 (8*i)           -- movsd %rdi[i] %x
    loadArg  (Reg x) i             = emit [0xf2, 0x0f, 0x5a, 0x86 .|. shiftL x 3] >> emitI32 (8*i)  -- cvtsd2ss %x %rsi[i]
    loadTemp (Reg x) (Temporary i) = emit [0xf3, 0x0f, 0x10, 0x82 .|. shiftL x 3] >> emitI32 (4*i)  -- movss %x %rdx[i]
    condRet = epilogue GradientIsNeeded

    getTmpMap = Asm $ gets as_temps
    spillTemp (Reg x) = do i <- Asm $ state $ \as -> (as_n_temps as, as { as_n_temps = as_n_temps as + 1 })
                           emit [0xf3, 0x0f, 0x11, 0x82 .|. shiftL x 3] >> emitI32 (4*i)              -- movss %rdx[i] %x
                           return (Temporary i)
    allocConst x = Asm $ do
            cm <- gets as_consts
            liftIO (H.lookup cm x) >>= \case
                Just  i -> return i
                Nothing -> do i <- gets $ Constant . as_n_csts
                              liftIO $ H.insert cm x i
                              modify $ \as -> as { as_n_csts = 1 + as_n_csts as }
                              return i

data VM' = VM' { registers   :: Z.Seq Float
               , vm_eax      :: Int32
               , vm_ecx      :: Int32
               , constants   :: Z.Seq Float
               , temporaries :: Z.Seq Float
               , results     :: Z.Seq Float
               , temptab     :: TmpMap
               , arguments   :: W.Vector Double }

newtype VM a = VM { runVM :: StateT VM' IO a }
    deriving ( Functor, Applicative, Monad, MonadIO )


evalAsm :: VM () -> W.Vector Double -> IO (Double, W.Vector Double)
evalAsm prg args = do
    tmp_map <- H.new
    let vm0 = VM'  { registers   = Z.replicate 16 0
                   , vm_eax      = 0
                   , vm_ecx      = 0
                   , constants   = Z.empty
                   , temporaries = Z.empty
                   , results     = Z.replicate (W.length args) 0
                   , temptab     = tmp_map
                   , arguments   = args }
    bimap float2Double (W.fromList . map float2Double . toList . results) <$>
        runStateT (runVM (prg >> getReg (Reg 0))) vm0

instance MonadAsm VM where
    addR rx ry = liftM2 (+) (getReg rx) (getReg  ry) >>= putReg rx
    addC rx  c = liftM2 (+) (getReg rx) (getConst c) >>= putReg rx
    subR rx ry = liftM2 (-) (getReg rx) (getReg  ry) >>= putReg rx
    mulR rx ry = liftM2 (*) (getReg rx) (getReg  ry) >>= putReg rx
    mulC rx  c = liftM2 (*) (getReg rx) (getConst c) >>= putReg rx
    divR rx ry = liftM2 (/) (getReg rx) (getReg  ry) >>= putReg rx
    movR rx ry = getReg ry >>= putReg rx
    maxC rx  c = liftM2 max (getReg rx) (getConst c) >>= putReg rx
    minC rx  c = liftM2 min (getReg rx) (getConst c) >>= putReg rx
    flrR rx ry = getReg ry >>= putReg rx . fromIntegral . (floor :: Float -> Int32)

    cvtToEax   rx = getReg rx >>= \x -> VM $ modify (\vm -> vm { vm_eax = truncate x })
    movToEax   rx = getReg rx >>= \x -> VM $ modify (\vm -> vm { vm_eax = fromIntegral $ castFloatToWord32 x })
    movToEcx   rx = getReg rx >>= \x -> VM $ modify (\vm -> vm { vm_ecx = fromIntegral $ castFloatToWord32 x })
    movFromEax rx = VM (gets vm_eax) >>= putReg rx . castWord32ToFloat . fromIntegral
    cvtFromEax rx = VM (gets vm_eax) >>= putReg rx . fromIntegral
    cvtFromEcx rx = VM (gets vm_ecx) >>= putReg rx . fromIntegral

    andEax  x = VM $ modify (\vm -> vm { vm_eax = vm_eax vm .&. x })
    orEax   x = VM $ modify (\vm -> vm { vm_eax = vm_eax vm .|. x })

    allocConst v = VM $ state (\vm -> ( Constant $ Z.length (constants vm), vm { constants = constants vm Z.|> v } ))
    loadC   rx c = getConst c >>= putReg rx

    getTmpMap        = VM $ gets temptab
    loadArg     rx i = VM (gets ((W.! i) . arguments)) >>= putReg rx . double2Float
    storeResult i rx = getReg rx >>= \x -> VM . modify $ \vm -> vm { results = Z.update i x (results vm) }
    condRet          = return () -- always compute gradient

    spillTemp rx = getReg rx >>= \v ->
                   VM (state (\vm -> ( Temporary $ Z.length (temporaries vm), vm { temporaries = temporaries vm Z.|> v } )))
    loadTemp rx (Temporary i) = VM (gets (fromJust . Z.lookup i . temporaries)) >>= putReg rx


getConst :: Constant -> VM Float
getConst (Constant c) = VM $ gets (fromJust . Z.lookup c . constants)

getReg :: Reg -> VM Float
getReg (Reg i) = VM $ gets (fromJust . Z.lookup (fromIntegral i) . registers)

putReg :: Reg -> Float -> VM ()
putReg (Reg i) x = VM . modify $ \vm -> vm { registers = Z.update (fromIntegral i) x (registers vm) }


printAsm :: PrintAsm () -> IO ()
printAsm asm = H.new >>= runReaderT (runPrintAsm asm)

newtype PrintAsm a = PrintAsm { runPrintAsm :: ReaderT TmpMap IO a }
    deriving ( Functor, Applicative, Monad, MonadIO )

-- XXX allocations return identifiers, these should be tracked and printed
instance MonadAsm PrintAsm where
    addR x y = liftIO $ putStrLn ("add " ++ show x ++ " " ++ show y)
    addC x y = liftIO $ putStrLn ("add " ++ show x ++ " " ++ show y)
    subR x y = liftIO $ putStrLn ("sub " ++ show x ++ " " ++ show y)
    mulR x y = liftIO $ putStrLn ("mul " ++ show x ++ " " ++ show y)
    mulC x y = liftIO $ putStrLn ("mul " ++ show x ++ " " ++ show y)
    divR x y = liftIO $ putStrLn ("div " ++ show x ++ " " ++ show y)
    movR x y = liftIO $ putStrLn ("mov " ++ show x ++ " " ++ show y)
    maxC x y = liftIO $ putStrLn ("max " ++ show x ++ " " ++ show y)
    minC x y = liftIO $ putStrLn ("min " ++ show x ++ " " ++ show y)
    flrR x y = liftIO $ putStrLn ("floor " ++ show x ++ " " ++ show y)

    cvtToEax   x = liftIO $ putStrLn ("cvttss2si %eax " ++ show x)
    movToEax   x = liftIO $ putStrLn ("movd %eax " ++ show x)
    movToEcx   x = liftIO $ putStrLn ("movd %ecx " ++ show x)
    movFromEax x = liftIO $ putStrLn ("movd " ++ show x ++ " %eax")
    cvtFromEax x = liftIO $ putStrLn ("cvtsi2ss " ++ show x ++ " %eax")
    cvtFromEcx x = liftIO $ putStrLn ("cvtsi2ss " ++ show x ++ " %ecx")

    andEax x = liftIO $ putStrLn ("and %eax 0x" ++ showHex x [])
    orEax  x = liftIO $ putStrLn ("or  %eax 0x" ++ showHex x [])

    allocConst x = liftIO $ putStrLn ("alloc " ++ show x) >> return (Constant 0)
    loadC      x y = liftIO $ putStrLn ("load " ++ show x ++ " " ++ show y)

    getTmpMap       = PrintAsm ask
    loadArg     x i = liftIO $ putStrLn ("load "  ++ show x ++ " arg[" ++ show i ++ "]")
    storeResult i x = liftIO $ putStrLn ("store " ++ show x ++ " grd[" ++ show i ++ "]")
    condRet         = liftIO $ putStrLn ("return if !grd")

    spillTemp  x = liftIO $ putStrLn ("spill " ++ show x) >> return (Temporary 0)
    loadTemp x _ = liftIO $ putStrLn ("load " ++ show x ++ " tmp")




-- Assembly to compute approximate log, see
-- http://www.machinedlearnings.com/2011/06/fast-approximate-logarithm-exponential.html
--
-- Expects the argument in (Reg 0), leaves the result in (Reg 0).
-- Clobbers (Reg 1), and also %eax and %ecx (these should always be
-- unused).
--
-- Original C code:
--
--       float fastlog2 (float x)
--       {
--         union { float f; uint32_t i; } vx = { x };
--         union { uint32_t i; float f; } mx = { (vx.i & 0x007FFFFF) | 0x3f000000 };
--         float y = vx.i;
--         y *= 1.1920928955078125e-7f;
--
--         return y - 124.22551499f
--                  - 1.498030302f * mx.f
--                  - 1.72587999f / (0.3520887068f + mx.f);
--       }
--
--       float fastlog (float x)
--       {
--         return 0.69314718f * fastlog2 (x);
--       }

emitLog :: MonadAsm m => m ()
emitLog = do
    movToEax (Reg 0)                            -- %eax  := cast_to_int x
    movToEcx (Reg 0)                            -- %ecx  := cast_to_int x
    andEax 8388607
    orEax 1056964608                            -- %ecx  := mx.i

    lc4 <- allocConst 1.72587999
    loadC (Reg 1) lc4                           -- %xmm1 := LC4
    movFromEax (Reg 0)                          -- %xmm0 := cast_to_float mx.i == mx.f

    lc3 <- allocConst 0.3520887068
    addC (Reg 0) lc3                            -- %xmm0 = mx.f + LC3
    divR (Reg 1) (Reg 0)                        -- %xmm1 := LC4 / (mx.f + LC3)

    movFromEax (Reg 0)                          -- %xmm0 := cast_to_float mx.i == mx.f
    lc2 <- allocConst 1.498030302
    mulC (Reg 0) lc2                            -- %xmm0 := mx.f * LC2
    addR (Reg 1) (Reg 0)                        -- %xmm1 := 1.49*mx.f + 1.72/(0.35+mx.f)

    cvtFromEcx (Reg 0)                          -- %xmm0 := y
    lc0 <- allocConst  1.1920928955078125e-7
    mulC (Reg 0) lc0                            -- %xmm0 := y * LC0

    lc1 <- allocConst  124.22551499
    addC (Reg 1) lc1                            -- %xmm1 := 124 + 1.49*mx.f + 1.72/(0.35+mx.f)
    subR (Reg 0) (Reg 1)                        -- subss   %xmm1, %xmm0

    lc5 <- allocConst 0.69314718
    mulC (Reg 0) lc5


-- Assembly to compute approximate exp, see
-- http://www.machinedlearnings.com/2011/06/fast-approximate-logarithm-exponential.html
--
-- Expects the argument in (Reg 0), leaves the result in (Reg 0).
-- Clobbers %xmm1, %xmm2, %xmm3; and also %eax (this should always be unused).
--
-- The C code uses cast-to-int and the logic around 'offset' to
-- implement floor().  We sidestep this and keep the control flow simple
-- by using 'Floor' directly.  The interpreter calls 'floor', the
-- assembler uses \"roundss\" with the appropriate rounding mode
-- (requires SSE 4.1, not a problem in 2018).
--
--      float fastexp (float p)
--      {
--        float p2 =  1.442695040f * p;
--
--        float offset = (p2 < 0) ? 1.0f : 0.0f;
--        float clipp = (p2 < -126) ? -126.0f : p2;
--        int w = clipp;
--        float z = clipp - w + offset;
--        union { uint32_t i; float f; } v =
--          { (uint32_t) ( (1 << 23) * (clipp + 121.2740575f + 27.7280233f / (4.84252568f - z) - 1.49012907f * z) ) };
--
--        return v.f;
--      }

emitExp :: MonadAsm m => m ()
emitExp = do
    subR (Reg 1) (Reg 1)            -- %xmm1 := 0

    lcA <- allocConst 1.442695040
    mulC (Reg 0) lcA                -- %xmm0 := 1.44 * p == p2

    lc3 <- allocConst  (-126)
    maxC (Reg 0) lc3                -- %xmm0 := max( -126, %xmm0 )
    lc3' <- allocConst 126
    minC (Reg 0) lc3'               -- %xmm0 := min( 126, %xmm0 ) == clipp

    movR (Reg 1) (Reg 0)            -- %xmm1 := %xmm0 == clipp

    flrR (Reg 2) (Reg 0)            -- %xmm2 := (floor)%xmm0 = (float)w-offset
    subR (Reg 1) (Reg 2)            -- %xmm1 := %xmm1 - %xmm2 = clipp - (float)(w-offset) = z

    lc4 <- allocConst 121.2740575
    loadC (Reg 2) lc4
    addR (Reg 2) (Reg 0)            -- %xmm2 := 121 + %xmm0  == 121 + clipp

    lc5 <- allocConst 4.84252568
    loadC (Reg 3) lc5               -- %xmm3 := 4.84
    lc6 <- allocConst 27.7280233
    loadC (Reg 0) lc6               -- %xmm0 := 27.7
    subR (Reg 3) (Reg 1)            -- %xmm3 = %xmm3 - %xmm1 = 4.84 - z

    lc7 <- allocConst 1.49012907
    mulC (Reg 1) lc7                -- %xmm1 := %xmm1 * 1.49 = 1.49 * z
    divR (Reg 0) (Reg 3)            -- %xmm0 := %xmm0 / %xmm3 = 27.7 / (4.84 - z)

    addR (Reg 0) (Reg 2)            -- %xmm0 := %xmm0 + %xmm2 = clipp + 121 + 27.7/(4.84-z)
    subR (Reg 0) (Reg 1)            -- %xmm0 := %xmm0 - %xmm1 = clipp + 121 + 27.7/(4.84-z) - 1.49*z
    lc8 <- allocConst (2^(23::Int))
    mulC (Reg 0) lc8                -- %xmm0 := %xmm0 * 2^23 = (1<<23) * (clipp + 121 + 27.7/(4.84-z) - 1.49*z)

    cvtToEax (Reg 0)                -- %rax := v.i
    movFromEax (Reg 0)              -- %xmm0 := v.f

