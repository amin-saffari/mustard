{-# LANGUAGE UndecidableInstances, Rank2Types, MagicHash #-}
-- | A Computer Algebra System
--
-- We will have to substitute terms, compute deep derivatives, and simplify
-- the resulting terms.  Might as well try to do it properly, and maybe
-- we can even add the inverse Laplace transform.
--
-- The idea (this time) is to build an ordinary Haskell data structure,
-- taking care to share subexpressions whereever possible.  (Sharing is
-- crucial when constructing deep partial derivatives.)  An evaluator or
-- compiler can observe the sharing through abuse of
-- 'System.Mem.StableName.makeStableName' to generate code of tractable
-- complexity.
--
-- If anything needs to be memoized, it has to be attached to the main
-- data structure, and in such a way that sharing is preserved.  To this
-- end, we annotate every subterm with a lazy list of its derivatives
-- and a lazy list of versions where the dummy variables are
-- substituted.  (Another substitution may need to be added if we want
-- to ignore a branch.  Hope that scales to where we need it...)

module Cas where

import BasePrelude                  hiding ( (:+), try, many, some, Sum, option, Void )
import Control.Monad.Reader
import Control.Monad.ST.Unsafe             ( unsafeIOToST )
import Control.Monad.State.Strict
import Data.Ratio                          ( numerator, denominator )
import Data.Text                           ( Text )
import Data.Text.Encoding                  ( decodeUtf8 )
import Data.Text.Lazy.Encoding             ( encodeUtf8 )
import Data.Void
import Number
import System.IO                           ( hPutStrLn, stderr, stdout )
import Text.Megaparsec              hiding ( State )
import Text.Megaparsec.Char

import qualified Data.ByteString                    as B ( readFile )
import qualified Data.ByteString.Lazy.Char8         as B ( hPut, unlines )
import qualified Data.HashTable.ST.Basic            as H
import qualified Data.IntMap.Strict                 as I
import qualified Data.Map.Strict                    as M
import qualified Data.Text.Lazy                     as T
import qualified Data.Text.Lazy.Builder             as B
import qualified Data.Text.Lazy.Builder.Int         as B
import qualified Data.Text.Lazy.Builder.RealFloat   as B
import qualified Data.Vector.Storable               as W
import qualified Text.Megaparsec.Char.Lexer         as L

-- | Names for variables.  We use negative indices for dummy variables,
-- positive indices for \"business\" variables.  @Name 0@ shall be the
-- mutation rate µ.  When evaluating expressions, the dummies should be
-- gone and only \"business\" variables are passed in.  The table can
-- (and should) be prepopulated.
newtype Name = Name Int deriving ( Eq, Ord, Storable )

instance Show Name where
    show (Name i) | i == 0    = "µ"
                  | i > 0     = "x_{" ++ shows   i  "}"
                  | otherwise = "ω_{" ++ shows (-i) "}"

instance IsString Expr where
    fromString "µ" = varE (Name 0)
    fromString "x" = varE (Name 0)
    fromString "y" = varE (Name 1)
    fromString "z" = varE (Name 2)
    fromString ('x':ds) | [(i,[])] <- reads $ filter isDigit ds = varE (Name i)
    fromString ('ω':ds) | [(i,[])] <- reads $ filter isDigit ds = varE (Name (-i))
    fromString ('w':ds) | [(i,[])] <- reads $ filter isDigit ds = varE (Name (-i))
    fromString s = error $ "can't turn variable " ++ show s ++ " into expression"

-- | Expressions.  Instead of subtraction, we use addition and
-- multiplication with (-1), instead of division, we use multiplication
-- and exponentiation with (-1).  Powers are limited to integer powers
-- of arbitrary expressions, but the exponential function takes any
-- argument.  The constant zero should be represented as empty sum; any
-- other constant is a scaled empty product.
--
-- It might be beneficial to enforce a certain normal form; say every
-- expression is a sum, each summand is a product, each factor is a
-- power of an expression.  That might help with simplification, or it
-- might not...

data ExprP = Prod  [Expr] !Number    -- scaled product; scale shall not be zero
           | Var   !Name         -- a named variable
           | Log    Expr          -- natural log
           | Exp    Expr          -- exponential
           | Sum   [Expr]         -- empty sum is zero
           | Pow    Expr  !Int
  deriving ( Show, Eq, Ord )

-- | Annotations to expressions serve to memoize related expressions.
data Expr = ExprA { prim_expr :: ExprP        -- the expression
                  , grad_vars :: [Expr]      -- lazy gradient for vars 0..
                  , grad_dums :: [Expr]      -- lazy gradient for vars -1,-2,..
                  , subst_mu :: [Expr] }  -- lazy variable substitutions

instance Eq Expr where
    ExprA a _ _ _ == ExprA b _ _ _ = a == b

instance Ord Expr where
    ExprA a _ _ _ `compare` ExprA b _ _ _ = a `compare` b

instance Show Expr where
    show = show . prim_expr

-- | Gradient.  Needs to know how many variables to consider, otherwise
-- it would result in an infinite and expensive list of zeroes.
gradient :: Vars -> Expr -> [Expr]
gradient vs = take n . grad_vars
  where
    n = maybe 0 (succ . fst . fst) $ I.maxViewWithKey vs

-- | 'derive x e' returns the partial derivative of 'e' with respect to
-- 'x'.  It returns the already memoized term.
derive :: Name -> Expr -> Expr
derive (Name i) e
    | i >= 0    = grad_vars e !! i
    | otherwise = grad_dums e !! (-i-1)

-- $ Smart constructors build expressions, simplify them as far as that
-- is cheaply possible, and annotate them with derivatives and prepared
-- substitutions.  All of them fold constants, sumE and prodE order
-- their arguments and try to join \"like\" arguments into simpler
-- expressions.  The simplifications should all be cheap, certainly no
-- worse than O(n log n).

zeroE :: Expr
zeroE = ExprA (Sum []) (repeat zeroE) (repeat zeroE) (repeat zeroE)

oneE :: Expr
oneE = constE 1

constE :: Number -> Expr
constE 0 = zeroE
constE x = self
  where
    self = ExprA (Prod [] x) (repeat zeroE) (repeat zeroE) (repeat self)

-- We use @exp µ@ instead of plain @µ@, because that avoids the boundary
-- condition at @µ==0@.  (It also turns the uninformative prior @dµ/µ@
-- into the uniform prior @dµ@, which might be easier to work with.)
muE :: Expr
muE = expE $ varE (Name 0)

varE :: Name -> Expr
varE (Name x) = self
  where
    self = ExprA (Var (Name x))
                 [ if x == y then oneE else zeroE | y <- [0..] ]
                 [ if x == y then oneE else zeroE | y <- [-1,-2..] ]
                 [ if x == y then  muE else self  | y <- [-1,-2..] ]

scaleE :: Number -> Expr -> Expr
scaleE 1 e = e
scaleE c e = case prim_expr e of
    Prod  es d             -> prodE (c*d) es
    Sum   es               -> sumE $ map (scaleE c) es
    _                      -> self
  where
    self = ExprA { prim_expr = Prod [e] c
                 , grad_vars = map (scaleE c) (grad_vars e)
                 , grad_dums = map (scaleE c) (grad_dums e)
                 , subst_mu  = map (scaleE c) (subst_mu  e) }

-- Turning a product into a sum of logs misbehaves; probably due to low
-- precision.  Since it would also be expensive in general, we don't do
-- that.  Pulling a constant out of the log works and rarely opens up an
-- opportunity for further simplification; it looks ugly, though.
logE :: Expr -> Expr
logE e =
    case prim_expr e of
        Prod fs c | c /= 1 -> constE (Double . log $ n2d c) + logE (prodE 1 fs)
        Exp     x          -> x
        Pow   x n          -> scaleE (fromIntegral n) (logE x)
        _                  -> self
  where
    self = ExprA { prim_expr = Log e
                 , grad_vars = [ de / e | de <- grad_vars e ]
                 , grad_dums = [ de / e | de <- grad_dums e ]
                 , subst_mu  = [ logE x | x  <- subst_mu  e ] }

expE :: Expr -> Expr
expE e =
    case prim_expr e of
        Prod [] (Double f) -> constE . Double . exp $     f
        Prod [] (Rat    f) -> constE . Double . exp $ r2d f
        Log             x  -> x
        _                  -> self
  where
    self = ExprA { prim_expr = Exp e
                 , grad_vars = [ self * de | de <- grad_vars e ]
                 , grad_dums = [ self * de | de <- grad_dums e ]
                 , subst_mu  = [ expE x    | x  <- subst_mu  e ] }

infixl 8 `powE`
powE :: Expr -> Int -> Expr
powE _ 0 = oneE
powE e 1 = e
powE e n =
    case prim_expr e of
        Sum  [] | n >= 0 -> zeroE
        Exp     x        -> expE $ scaleE (fromIntegral n) x
        Pow   x m        -> powE x (m*n)
        Prod xs c        -> prodE (c^^n) (map (`powE` n) xs)
        _                -> self
  where
    self = ExprA { prim_expr = Pow e n
                 , grad_vars = [ scaleE (fromIntegral n) (powE e (n-1)) * de | de <- grad_vars e ]
                 , grad_dums = [ scaleE (fromIntegral n) (powE e (n-1)) * de | de <- grad_dums e ]
                 , subst_mu  = [ powE x n                                    | x  <- subst_mu  e ] }

-- | Smart sum constructor.  Each summand is normalized to a scaled
-- product.  Summands are sorted, then the scale factors of \"like\"
-- arguments are added.  It's finally passed to 'cfactor', which
-- attempts to factor a constant out.  Statically reciognized zeroes are
-- dropped.
--
-- Logarithms are not treated separately.  It's probably unwise to turn
-- sums of logs into the log of a huge product, because it is certain to
-- overflow.
--
-- Instead of considering only scaled products, we could handle
-- arbitrary products and try to factor and add up the lexically
-- smallest factor.  This requires a different ordering of products.

sumE :: [Expr] -> Expr
sumE = cfactor . combineLike . sort . flattenSums

flattenSums :: [Expr] -> [( Expr, Number )]
flattenSums = go 1 []
  where
    go _ acc [    ] = acc
    go s acc (n:ns) = case prim_expr n of
            Prod [f] c -> case prim_expr f of
                    Sum xs -> go s (go (s*c) acc xs) ns
                    _      -> go s ((f,s*c) : acc) ns
            Prod fs c -> go s ((prodE 1 fs,s*c) : acc) ns
            Sum xs -> go s (go s acc xs) ns
            _      -> go s ((n,s) : acc) ns

-- | Combines adjacent \"like\" terms by adding their weights.
-- (In practice, weights are either scale factors or exponents.)
combineLike :: (Num a, Eq a) => [( Expr, a )] -> [( Expr, a )]
combineLike ((x,e):(y,f):as) | x == y = combineLike ((x,e+f):as)
combineLike ((_,0)      :as)          = combineLike as
combineLike ((x,e)      :as)          = (x,e) : combineLike as
combineLike [              ]          = []

-- | Sums a list of scaled expressions, but tries to pull out a constant
-- factor.  If the first summand is a product, its scale factor is pulled
-- out.  (This neatly subsumes constants, which have the form
-- @Prod c []@.)  Else we just apply 'Sum'.
cfactor :: [( Expr, Number )] -> Expr
cfactor [     ] = zeroE
cfactor [(e,c)] = scaleE c e
cfactor ((e,c):es) = prodE c [self]
  where
    es'  = e : map (\(x,s) -> scaleE (s/c) x) es
    self = ExprA { prim_expr = Sum es'
                 , grad_vars = map sumE $ transpose $ map grad_vars es'
                 , grad_dums = map sumE $ transpose $ map grad_dums es'
                 , subst_mu  = map sumE $ transpose $ map subst_mu  es' }


-- | Normalize a product, attempting to simplify it.  We want to turn
-- @x^i * x^j@ into @x^(i+j)@ and @e^x * e^y@ into @e^(x+y)@, and we
-- must collect constants.  The first must work at least for powers of
-- variables, but arbitrary expressions would be good.
--
-- Possible strategy:  we separately collect constants, exponentials,
-- and everything else.  "Everything else" is always expressed as @x^i@
-- with @i=1@ for any expression that wasn't already of this shape.
-- Then sort, combine using smart constructors, drop ones, reassemble.
-- XXX  Here be dragons.
prodE :: Number -> [Expr] -> Expr
prodE c [ ] = constE c
prodE 1 [x] = x
prodE c  xs = self
  where
    (c', exps, prods) = flattenProds c xs
    e' = case exps of [ ] -> id
                      [e] -> (expE e :)
                      _   -> (expE (sumE exps) :)
    fs = e' . map (uncurry powE) . combineLike . sort $ prods

    self = ExprA { prim_expr = Prod fs c'
                 , grad_vars = map prod_deriv $ transpose $ map grad_vars fs
                 , grad_dums = map prod_deriv $ transpose $ map grad_dums fs
                 , subst_mu  = map (prodE c)  $ transpose $ map subst_mu  fs }

    -- d/dx a1 * a2 * ... * an
    --      = d/dx a1 * a2 * ... * an
    --      + a1 * d/dx a2 * a3 * ... * an
    --      + ...
    --      + a1 * a2 * ... * a{n-1} * d/dx an

    -- argument is [d/dx a1, d/dx a2, ..., d/dx an]
    prod_deriv ds = sumE $ zipWith3 (\l m r -> prodE c' $ l ++ [m] ++ r)
                                    (init $ inits fs) ds (tail $ tails fs)

-- | Flattens nested products, collects constants and exponentials
-- separately from powers of radicals.
-- XXX  Here be dragons.
flattenProds :: Number -> [Expr] -> ( Number, [Expr], [( Expr, Int )] )
flattenProds cst0 = go 1 cst0 [] []
  where
    go  _  !cst exps pows [    ] = (cst, exps, pows)
    go !ex !cst exps pows (n:ns) = case prim_expr n of
            -- early out as soon as we hit a zero
            Sum   [] -> (0,[],[])
            Prod _ 0 -> (0,[],[])

            Pow x p -> case prim_expr x of
                    Prod xs c -> case go (ex*p) (c^^(ex*p) * cst) exps pows xs of
                                    (cst', exps', pows') -> go ex cst' exps' pows' ns
                    Exp y -> go (ex*p) cst (scaleE (fromIntegral (ex*p)) y : exps) pows ns
                    _     -> go ex cst exps ((x,ex*p) : pows) ns

            Prod xs c -> case go ex (c^^ex * cst) exps pows xs of
                            (cst', exps', pows') -> go ex cst' exps' pows' ns
            Exp x -> go ex cst (scaleE (fromIntegral ex) x : exps) pows ns
            _     -> go ex cst exps ((n,ex) : pows) ns


-- | @substMu v e@ substitutes @µ@ for the dummy variable @v@ everywhere
-- in @e@.  It returns the already memoized term.  (That's also why @µ@
-- is special and it only works for dummies: it's the only kind of
-- substitution we memoize.)
substMu :: Name -> Expr -> Expr
substMu (Name v) e
    | v < 0 = subst_mu e !! (-v-1)
    | otherwise = error "cannot not substitute business variable"

instance Num Expr where
    fromInteger = constE . fromIntegral
    x + y       = sumE    [x, y]
    x - y       = sumE    [x, scaleE (-1) y]
    x * y       = prodE 1 [x, y]
    negate x    = scaleE (-1) x
    abs x       = x
    signum _    = oneE

instance Fractional Expr where
    fromRational = constE . fromRational
    x / y   = prodE 1 [x, powE y (-1)]
    recip x = powE x (-1)

instance Floating Expr where
    log = logE
    exp = expE

type Vars = I.IntMap Text
data Env = Env !(M.Map Text Name) !Vars
type Parser m a = MonadState Env m => ParsecT Void Text m a


-- | Runs a computation with predefined variables.  In @withVars vs ds@,
-- @vs@ gives names to the \"business\" variables and @ds" gives names
-- to the dummy variables.  The first \"business\" variable is special,
-- it is the mutation rate.
withVars :: Monad m => [Text] -> [Text] -> StateT Env m r -> m (r, Vars)
withVars vs ds k = do (r, Env _ v) <- runStateT k (Env t2v v2t) ; return (r,v)
  where
    t2v = M.fromList $ zip vs (map Name [0..]) ++ zip ds (map Name [-1,-2..])
    v2t = I.fromList $ zip [0..] vs ++ zip [-1,-2..] ds


findVar :: MonadState Env m => Text -> m Name
findVar t =
    state (\ (Env t2v v2t) ->
        case M.lookup t t2v of
            Just nm -> (nm, Env t2v v2t)
            Nothing ->
                let nm = maybe 0 (succ . fst . fst) $ I.maxViewWithKey v2t
                in (Name nm, Env (M.insert t (Name nm) t2v) (I.insert nm t v2t)))

findDum :: MonadState Env m => Text -> m Name
findDum t =
    state (\ (Env t2v v2t) ->
        case M.lookup t t2v of
            Just nm -> (nm, Env t2v v2t)
            Nothing ->
                let nm = maybe (-1) (pred . fst . fst) $ I.maxViewWithKey v2t
                in (Name nm, Env (M.insert t (Name nm) t2v) (I.insert nm t v2t)))

sc :: Parser m ()
sc = L.space space1   -- space comsumer
             empty    -- line comment (relevant?)
             empty    -- block comment (relevant?)

lx :: Parser m a -> Parser m a
lx = L.lexeme sc

sym :: Text -> Parser m Text
sym = L.symbol sc

parse_from_file :: (MonadState Env m, MonadIO m) => FilePath -> m Expr
parse_from_file fp = do
    pp <- runParserT parse_expr fp . decodeUtf8 =<< liftIO (B.readFile fp)
    case pp of Left bundle -> liftIO $ hPutStrLn stderr (errorBundlePretty bundle) >> exitFailure
               Right     e -> return e

-- | Parses Mathematica expressions, or at least a useful subset of
-- them.  CSE is achieved on the fly through smart constructors that
-- memoize and recycle everything.
parse_expr :: Parser m Expr
parse_expr = esum
  where
    esum  = chainl1 eprod ((+) <$ sym "+" <|> (-) <$ sym "-")
    eprod = chainl1 epow  ((*) <$ sym "*" <|> (/) <$ sym "/")
    epow  = choice [ negate <$ sym "-" <*> epow
                   , try (string "E" >> notFollowedBy alphaNumChar) >> sc >> sym "^" >> parse_term >>= return . expE
                   , parse_term >>= \x -> option x (sym "^" >> lx L.decimal >>= return . powE x) ]

    chainl1 p op = p >>= rest
      where
        rest x = option x $ do f <- op ; y <- p ; rest (f x y)

parse_term :: Parser m Expr
parse_term = choice
    [ between (sym "(") (sym ")") parse_expr
    , lx (try L.float) >>= return . constE . Double
    , lx L.decimal >>= return . constE . i2n
    , lx param >>= lift . findVar >>= return . varE ]
  where
    param :: Parser m Text
    param = lx $ try $ do zs <- liftA2 (:) (letterChar <|> greekEscape)
                                           (many $ alphaNumChar <|> greekEscape)
                          guard $ zs /= "E"

                          ys <- option "" $ between (char '[') (char ']') $
                                    fmap ('_' :) $ some $
                                        letterChar <|> char '{' <|> char '}' <|>
                                        greekEscape <|> spaceChar <|> char ','
                          return . fromString . filter (not . isSpace) $ zs ++ ys

    greekEscape :: Parser m Char
    greekEscape = do z <- char '\\' >> between (char '[') (char ']') (some letterChar)
                     case z of "Omega"  -> return 'ω'
                               "Lambda" -> return 'Λ'
                               "Mu"     -> return 'µ'
                               _        -> fail $ "it's all greek to me: " ++ z

-- | Simple evaluator.  Useful for debugging, but probably too slow for
-- practical use, especially on deep derivatives.
evalExpr :: W.Vector Double -> Expr -> Double
evalExpr vs = go
  where
    go = go1 . prim_expr
    go1 (Var (Name x)) = vs W.! x
    go1 (Log        x) = log (go x)
    go1 (Exp        x) = exp (go x)
    go1 (Sum       xs) = foldl' (+)    0    $ map go xs
    go1 (Prod    xs c) = foldl' (*) (n2d c) $ map go xs
    go1 (Pow      x n) = go x ^^ n


type Eval s a = ReaderT (H.HashTable s (StableName Expr) Double, W.Vector Double) (ST s) a

runEval :: (forall s . Eval s a) -> W.Vector Double -> a
runEval m args = runST (do mem <- H.new ; runReaderT m (mem, args))

-- | Evaluator for memoized subexpressions.  More useful than
-- 'evalExpr', but probably still too slow for practical use.
evalMemo :: Expr -> Eval s Double
evalMemo e = do (mem, args) <- ask
                nm <- lift . unsafeIOToST $ makeStableName e
                lift (H.lookup mem nm) >>= \case
                    Just  v -> return v
                    Nothing -> do v <- evalP args $ prim_expr e
                                  lift $ H.insert mem nm v
                                  return v
  where
    evalP as (Var (Name p)) = return $ as W.! p
    evalP  _ (Log        x) = log    <$> evalMemo x
    evalP  _ (Exp        x) = exp    <$> evalMemo x
    evalP  _ (Pow      x n) = (^^ n) <$> evalMemo x
    evalP  _ (Sum       xs) = foldM (\b a -> (b +) <$> evalMemo a)    0    xs
    evalP  _ (Prod    xs c) = foldM (\b a -> (b *) <$> evalMemo a) (n2d c) xs


-- Shows an expression in a readable way.  This is as pretty as
-- possible, it is not proper Haskell syntax.
showE :: Vars -> Expr -> B.Builder
showE vars = go (0::Int)
  where
    showC1 1 = id
    showC1 x = (:) (showC x)

    showC (Double x) = B.realFloat x
    showC (Rat    x) | denominator x == 1 = B.decimal (numerator x)
                     | otherwise          = B.decimal (numerator x) <> "/" <> B.decimal (denominator x)

    go p = go1 p . prim_expr
    go1 _ (Var (Name x)) = case I.findWithDefault "" x vars of
                                 "" | x == 0    -> "µ"
                                    | x >  0    -> "x_{" <> B.decimal x <> "}"
                                    | otherwise -> "ω_{" <> B.decimal (-x) <> "}"
                                 v -> B.fromText v

    go1 _ (Log        x) = "log " <> go 10 x
    go1 _ (Exp        x) = "e^" <> go 10 x
    go1 _ (Sum       []) = "0"
    go1 _ (Prod    [] c) = showC c
    go1 p (Sum       xs) = buildParen (p>6) $ mconcat $ intersperse " + " $ map (go 6) xs
    go1 p (Prod    xs c) = buildParen (p>7) $ mconcat $ intersperse " * " $ showC1 c $ map (go 7) xs
    go1 p (Pow      x n) = buildParen (p>8) $ go 8 x <> "^" <> B.decimal n

    buildParen False b = b
    buildParen True  b = "(" <> b <> ")"

----------------

-- | Dumps an expression to stdout, assuming the terminal uses UTF8
-- encoding.
printE :: Vars -> Expr -> IO ()
printE vars = B.hPut stdout . B.unlines . map encodeUtf8 . flow . B.toLazyTextWith 16368. showE vars
  where
    flow s = if T.length l < 120 then [l] else l' : flow r'
      where
        l  = T.take 120 s
        l' = T.dropWhileEnd (not . isSpace) l
        r' = T.drop (T.length l') s


{- Simple and stupid substitution:  every occurence of a variable is
-- replaced by an expression.  The result is simplified as far as
-- practical.
-- This function will lose sharing, so it's diabled for the moment.
substV :: MonadState Env m => Text -> m Name -> m Name -> m Name
substV n r e = do
    x <- gets $ fromJust (error ("no such variable: " ++ T.unpack n)) . M.lookup n . env_varmemo
    subst x r =<< e

-- | For testing only: substitute \\mu for a certain set of dummy variables.
substall :: MonadState Env m => m Name -> m Name
substall = foldr (.) id $ map (\i -> substV ("ω_{" <> i <> "}") (varE "μ")) $
                T.words "a b c d a,b a,c a,d b,c b,d c,d a,b,c a,b,d a,c,d b,c,d" -}
