int cpu_flags()
{
    return
#if __x86_64__
        2
#elif __i386__
        1
#else
        0
#endif
#if __x86_64__ || __i386__
        + (__builtin_cpu_supports("sse2")   ? 4 : 0)
        + (__builtin_cpu_supports("sse4.1") ? 8 : 0)
#endif
        ;
}
