# mustard

Experiments with the "Lohse Method" to evaluate likelihood
functions for phylogenetic models.

* Module "GenFunc" contains a brief exposition of the Lohse Method, with
  an explanation on how Automatic Differentiation can be used to apply
  it in practice.  It also implements a few generic functions.

* Module "Neandertard" implements a Generating Function for a model that
  has population joins, resurrection of an extinct species, and gene
  flow.  While it does have an application, it is meant as an example.

* Module "Bsfs" works with Block Site Frequency Spectra.  Right now, it
  does dumb stuff and will be rewritten.

* Modules "Cas" and "Number" implement a primitive Computer Algebra
  System.  It has symbolic expressions and can simplify them to some
  extent.  While it turned out that this isn't needed to apply the
  method, it may be great for debugging.

* Modules "Asm" and "AsmLL" implement a JIT-Compiler for the CAS.  It's
  not exactly production ready, but it works.  This, too, turned out to
  be unneeded.

* cg\_descent contains the conjugate gradient method by Hager and Zhang.
  We may end up using it for maximizing the likelihood, even though "ad"
  contains some clever algorithms of similar capability, too.
